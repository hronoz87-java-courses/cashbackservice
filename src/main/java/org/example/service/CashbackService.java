package org.example.service;

public class CashbackService {

    public int getCashback(int amount, int percent) {
        int maxSumAllCashback = 3000;
        int cashback = amount * percent / 100;
        if (cashback >= maxSumAllCashback) {
            return maxSumAllCashback;
        }
        return cashback;
    }
}
